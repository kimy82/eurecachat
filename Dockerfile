FROM node:argon

#WORKDIRECTORY
RUN mkdir -p /usr/src/eurecaChat
WORKDIR /usr/src/eurecaChat

#DEPENDENCIES
COPY package.json /usr/src/eurecaChat/
RUN npm install

#SOURCECODE
COPY . /usr/src/eurecaChat

RUN groupadd -r eureca
RUN useradd -r -g eureca eureca
RUN chown -R eureca:eureca /usr/src/eurecaChat
USER eureca

EXPOSE 8000
CMD [ "npm", "start" ]
