#EURECA.IO CHAT FUNCTIONALITY

##TECHNOLOGIES

* Eureca.io (Node lib)
* ExpressJS (MVC for node apps)
* Browserify (Converts node js file to a client js)
* Knockout
* Typescript
* less

##SETUP

* Clone repository
    * git clone https://kimy82@bitbucket.org/kimy82/eurecachat.git
* Install node modules
    * npm install
* Install typings
    * tsd install
* Run the application
    * node app.js
* Change chat.ts
    * _gulp browserify_ (wach for changes in chat.js and creates a raw js file in build folder)
* Change styles
    * Install less  "npm install -g less" and then _gulp less_

##Running tests
For the testing side we are using mocha, sinon and chai.
* In order to run the test run the following command:
    * _mocha test --recursive --watch_

##DocKer Instructions
* _docker build -t kimy82/eurecachat ._
* _docker login --username= --email=_
* _docker push kimy82/eurecachat_
* _docker run -p 8000:8000 kimy82/eurecachat

##Docker useful comments
* _docker ps_ (view active images)
* _docker ps -a_ (view all images)
* remove image
    * _docker stop pid_
    * _docker rm pid_
* Pul image from repo**
    * _docker pull kimy82/eurecachat_
