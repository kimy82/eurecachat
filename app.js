"use strict";
var config_1 = require('./config/config');
var connectionsHolder_1 = require('./utils/connectionsHolder');
var chatEngine_1 = require('./utils/chatEngine');
var EventEmitter = require('events');
config_1["default"].app.get('/', function (req, res, next) {
    res.render('index');
});
var disconnectionEventEmitter = new EventEmitter();
var connectionsHolder = new connectionsHolder_1["default"](config_1["default"].eurecaServer, disconnectionEventEmitter);
var chatEngine = new chatEngine_1["default"](connectionsHolder, disconnectionEventEmitter);
config_1["default"].eurecaServer.exports.tchatServer = { startChat: chatEngine.startChat,
    endChat: chatEngine.endChat,
    send: chatEngine.send };
config_1["default"].server.listen(8000);
console.log('Eureca.io tchat server listening on port 8000');
exports.App = config_1["default"].app;
//# sourceMappingURL=app.js.map