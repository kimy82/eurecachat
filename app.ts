import config from './config/config';
import ConnectionsHolder from './utils/connectionsHolder';
import ChatEngine from './utils/chatEngine';
const EventEmitter = require('events');

config.app.get('/', function(req, res, next) {
    res.render('index');
});

const disconnectionEventEmitter = new EventEmitter();

var connectionsHolder = new ConnectionsHolder(config.eurecaServer, disconnectionEventEmitter);

var chatEngine = new ChatEngine(connectionsHolder, disconnectionEventEmitter);

config.eurecaServer.exports.tchatServer = {startChat: chatEngine.startChat,
                                           endChat: chatEngine.endChat,
                                           send: chatEngine.send};

config.server.listen(8000);
console.log('Eureca.io tchat server listening on port 8000');

export var App = config.app;
