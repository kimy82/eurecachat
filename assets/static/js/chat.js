/// <reference path="../../../customTypings/customTypings.d.ts"/>
"use strict";
var _ = require("underscore");
var ko = require("knockout");
var moment = require("moment");
var kov = require("knockout.validation");
kov.init({ insertMessages: false });
/**
* Message Class
**/
var Message = (function () {
    function Message(name, message) {
        this.messageTemplate = _.template("<b class='margin-small-right'><%= name %></b><span><%=message%></span>");
        this.name = name;
        this.message = message;
        this.messageLine = this.messageTemplate({ name: name, message: message });
    }
    return Message;
}());
/**
* Client class
**/
var ClientLogged = (function () {
    function ClientLogged(id, name) {
        this.messages = ko.observableArray();
        this.messageNotSeen = ko.observable(false);
        this.clientTemplate = _.template("<b><%= name %></b>");
        this.name = name;
        this.id = id;
        this.clientLine = this.clientTemplate({ name: name });
    }
    return ClientLogged;
}());
/**
* Chat controller
**/
var ChatView = (function () {
    function ChatView() {
        var _this = this;
        this.clients = ko.observableArray();
        this.errorNoMessage = ko.observable(false);
        this.errorNoClient = ko.observable(false);
        this.clientSelected = ko.observable(null);
        this.client = new Eureca.Client();
        this.loginName = ko.observable("");
        this.messageToSend = ko.observable("").extend({ required: true });
        this.isLogged = ko.observable(false);
        this.login = function () {
            if (!_this.server)
                return; //client not ready
            _this.server.tchatServer.startChat(_this.loginName());
        };
        this.sendMessage = function () {
            if (!_this.server)
                return; //client not ready
            if (_this.clientSelected() == null) {
                _this.errorNoClient(true);
                return;
            }
            if (_this.messageToSend.isValid()) {
                console.log("Send message: " + _this.messageToSend() + " to " + _this.clientSelected().name);
                _this.errorNoMessage(false);
                _this.server.tchatServer.send('<i style=\'font-size: 10px; color: #b3b3b3;\'>' + moment(new Date()).format('MM/DD/YYYY hh:mm:ss') + '</i>  ' + _this.messageToSend(), _this.clientSelected().name);
                _this.messageToSend("");
            }
            else {
                _this.errorNoMessage(true);
            }
            _this._scrollMessagesToBottom();
        };
        this.onEnterSend = function (d, e) {
            if (e.keyCode === 13) {
                _this.sendMessage();
            }
            return true;
        };
        this.showMessages = function (client) {
            _this.errorNoClient(false);
            var clientSelected = ko.utils.arrayFirst(_this.clients(), function (cl) {
                return cl.name == client.name;
            });
            _this.clientSelected(clientSelected);
            _this.clientSelected().messageNotSeen(false);
            _this._scrollMessagesToBottom();
            console.log("Select user " + clientSelected.name);
        };
        this.quitChat = function () {
            _this.server.tchatServer.endChat();
            _this.clientSelected(null);
            _this.isLogged(false);
        };
        this.buildTchat = function () {
            _this.tchat = _this.client.exports.tchat = {
                send: function (name, message) {
                    var clientMatch = ko.utils.arrayFirst(_this.clients(), function (cl) {
                        return cl.name == name;
                    });
                    if (clientMatch != null) {
                        document.title = "New message!";
                        if (!window.isActive) {
                            var audio = new Audio('/audio/bell.mp3');
                            audio.volume = 0.1;
                            audio.play();
                        }
                        if (_this.clientSelected() == null || clientMatch.name != _this.clientSelected().name) {
                            clientMatch.messageNotSeen(true);
                        }
                        clientMatch.messages.push(new Message(name, message));
                    }
                    else if (_this.clientSelected() != null) {
                        _this.clientSelected().messages.push(new Message(name, message));
                    }
                    _this._scrollMessagesToBottom();
                },
                welcome: function () {
                    _this.isLogged(true);
                },
                isGone: function (id) {
                    _this.clients.remove(function (cl) { return cl.id == id; });
                },
                isConnected: function (id, name) {
                    var clientMatch = ko.utils.arrayFirst(_this.clients(), function (cl) {
                        return cl.name == name || cl.id == id;
                    });
                    if (!clientMatch)
                        _this.clients.push(new ClientLogged(id, name));
                }
            };
        };
        this.client.ready(function (proxy) { _this.server = proxy; });
        this.loginName('anonymous-' + new Date().getTime());
        this.buildTchat();
        window.onfocus = function () { window.isActive = true; };
        window.onblur = function () { window.isActive = false; };
    }
    ChatView.prototype._scrollMessagesToBottom = function () {
        $('.messages-box').animate({ scrollTop: $('.messages-box')[0].scrollHeight }, 100);
    };
    return ChatView;
}());
$(function () {
    ko.applyBindings(new ChatView());
    $(document).on('mouseover', function () { document.title = 'Eureca chat example'; });
});
exports.chat = { chatview: ChatView, clientLogged: ClientLogged };
module.exports = exports.chat;
//# sourceMappingURL=chat.js.map