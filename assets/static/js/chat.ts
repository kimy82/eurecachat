/// <reference path="../../../customTypings/customTypings.d.ts"/>

import * as _ from "underscore"
import * as ko from "knockout"
import * as moment from "moment"
var kov = require("knockout.validation");

kov.init({ insertMessages: false });

/**
* Message Class
**/
class Message {
    public name: string;
    public message: string;
    public messageLine;
    private messageTemplate = _.template("<b class='margin-small-right'><%= name %></b><span><%=message%></span>");

    constructor(name: string, message: string) {
        this.name = name;
        this.message = message;
        this.messageLine = this.messageTemplate({ name: name, message: message });
    }
}

/**
* Client class
**/
class ClientLogged {
    public name: string;
    public id: string;
    public messages: KnockoutObservableArray<Message> = ko.observableArray<Message>();
    public messageNotSeen: KnockoutObservable<boolean> = ko.observable<boolean>(false);
    public clientLine;
    private clientTemplate = _.template("<b><%= name %></b>");

    constructor(id: string, name: string) {
        this.name = name;
        this.id = id;
        this.clientLine = this.clientTemplate({ name: name });
    }
}

/**
* Chat controller
**/
class ChatView {

    public clients: KnockoutObservableArray<ClientLogged> = ko.observableArray<ClientLogged>();
    public errorNoMessage: KnockoutObservable<boolean> = ko.observable<boolean>(false);
    public errorNoClient: KnockoutObservable<boolean> = ko.observable<boolean>(false);
    public clientSelected: KnockoutObservable<ClientLogged> = ko.observable<ClientLogged>(null);
    public client = new Eureca.Client();
    public server;
    public loginName: KnockoutObservable<string> = ko.observable<string>("");
    public messageToSend: KnockoutObservable<string> = ko.observable<string>("").extend({ required: true });
    public isLogged: KnockoutObservable<boolean> = ko.observable<boolean>(false);
    public tchat: Tchat;
    constructor() {
        this.client.ready((proxy) => { this.server = proxy; });
        this.loginName('anonymous-' + new Date().getTime());
        this.buildTchat();
        window.onfocus = function () { (<any>window).isActive = true; };
        window.onblur = function () { (<any>window).isActive = false; };
    }

    public login = () => {
        if (!this.server) return; //client not ready
        this.server.tchatServer.startChat(this.loginName());
    }

    public sendMessage = () => {
        if (!this.server) return; //client not ready
        if (this.clientSelected() == null) {
            this.errorNoClient(true);
            return;
        }

        if (this.messageToSend.isValid()) {
            console.log("Send message: " + this.messageToSend() + " to " + this.clientSelected().name);
            this.errorNoMessage(false);
            this.server.tchatServer.send('<i style=\'font-size: 10px; color: #b3b3b3;\'>' + moment(new Date()).format('MM/DD/YYYY hh:mm:ss') + '</i>  ' + this.messageToSend(), this.clientSelected().name);
            this.messageToSend("");
        } else {
            this.errorNoMessage(true);
        }
        this._scrollMessagesToBottom();
    }

    public onEnterSend = (d, e) => {
        if (e.keyCode === 13) {
            this.sendMessage();
        }
        return true;
    }

    public showMessages = (client: ClientLogged) => {
        this.errorNoClient(false);
        var clientSelected = ko.utils.arrayFirst(this.clients(), (cl: ClientLogged) => {
            return cl.name == client.name;
        });
        this.clientSelected(clientSelected);
        this.clientSelected().messageNotSeen(false);
        this._scrollMessagesToBottom();
        console.log("Select user " + clientSelected.name);
    }

    public quitChat = () => {
        this.server.tchatServer.endChat();
        this.clientSelected(null);
        this.isLogged(false);
    }

    private buildTchat = () => {
        this.tchat = this.client.exports.tchat = {
            send: (name: string, message: string) => {
                var clientMatch = ko.utils.arrayFirst(this.clients(), (cl: ClientLogged) => {
                    return cl.name == name;
                });
                
                if (clientMatch != null) {
                    document.title = "New message!";
                    if(!(<any>window).isActive){var audio = new Audio('/audio/bell.mp3'); audio.volume=0.1; audio.play();}
                    if (this.clientSelected() == null || clientMatch.name != this.clientSelected().name) {
                        clientMatch.messageNotSeen(true);
                    }
                    clientMatch.messages.push(new Message(name, message));
                } else if (this.clientSelected() != null) {
                    this.clientSelected().messages.push(new Message(name, message));
                }
                this._scrollMessagesToBottom();
            },
            welcome: () => {
                this.isLogged(true);
            },
            isGone: (id) => {
                this.clients.remove((cl: ClientLogged) => { return cl.id == id; })
            },
            isConnected: (id: string, name: string) => {
                var clientMatch = ko.utils.arrayFirst(this.clients(), (cl: ClientLogged) => {
                    return cl.name == name || cl.id == id;
                });
                if (!clientMatch)
                    this.clients.push(new ClientLogged(id, name));
            }
        };
    }

    private _scrollMessagesToBottom(): void {
        $('.messages-box').animate({scrollTop: $('.messages-box')[0].scrollHeight},100);
    }
}

$(function() {
    ko.applyBindings(new ChatView());
    $(document).on('mouseover', function(){ document.title = 'Eureca chat example'; });
});

export var chat = { chatview: ChatView, clientLogged: ClientLogged };
module.exports = chat;
