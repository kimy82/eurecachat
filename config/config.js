"use strict";
var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var Eureca = require('eureca.io');
//EXPRESS APP//
var app = express();
app.set('views', 'assets');
app.use(express.static('assets/static'));
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({ extended: true }));
//EXPRESS APP END//
//EURECA CONFIG//
var eurecaServer = new Eureca.Server({ allow: ['tchat.welcome', 'tchat.send', 'tchat.isGone', 'tchat.isConnected'] });
var server = http.Server(app);
eurecaServer.attach(server);
//EURECA CONFIG END//
exports.config = { app: app, eurecaServer: eurecaServer, server: server };
exports.__esModule = true;
exports["default"] = exports.config;
//# sourceMappingURL=config.js.map