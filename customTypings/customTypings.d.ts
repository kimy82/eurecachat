/**
* Basic Eureca Server functions.
**/
interface EurecaServer {
    getClient(id: string): any;
    onConnect(callback: any);
    onDisconnect(callback: any);
}

/**
* Client side functions.
**/
interface Tchat {
  send(name:string, message:string);
  welcome();
  isGone(id:string);
  isConnected(id:string, name:string);
}

/**
* The main eureca client.
**/
interface EurecaClient {
  tchat: Tchat;
}

/**
* Tchat client info.
**/
interface Client {
    eurecaClient: EurecaClient;
    clientName: string;
    id: string;
}

/**
* Eureca server info.
**/
interface EurecaInfo {
    remoteAddress: string;
}

/**
* Basic eureca connection info.
**/
interface Connection {
    id: string;
    eureca: EurecaInfo;
}

/**
* ConnectionsHolder holding the client connections.
**/
interface ConnectionsHolder {
  eurecaServer: EurecaServer;
  connections: { [key:string]:Client; };
  configure();
}

/**
* TchatEngine functions to be called from the client side.
**/
interface TchatEngine {
  startChat(name: string);
  send(message: string);
  endChat();
}

/**
* This in tchat server funcations.
**/
interface thisConnection {
  connection:Connection;
}

interface EventEmitter {
  emit(eventName:string, arg1:any);
  on(eventName:string, callback:any);
}

/**
* Basic Eureca client side.
**/
interface EurecaStatic {
  Client():void;
}

declare var Eureca: EurecaStatic;
