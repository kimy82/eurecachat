var gulp = require('gulp');
var source = require('vinyl-source-stream'); // Used to stream bundle for further handling
var browserify = require('browserify');
var watchify = require('watchify');
var concat = require('gulp-concat');
var less = require('gulp-less');
var path = require('path');
var LessAutoprefix = require('less-plugin-autoprefix');
var autoprefix = new LessAutoprefix({
    browsers: ['last 9 Chrome versions', 'last 2 versions']
});


/**
 * Less compilation gulp task
 **/
gulp.task('less', function() {
    return gulp.src('./assets/static/less/**/*.less')
        .pipe(less({
            plugins: [autoprefix],
            paths: [path.join(__dirname, 'common')]
        }))
        .pipe(gulp.dest('./assets/static/css/'));
});

/**
 * Browserify gulp task.
 **/
gulp.task('browserify', function() {

    var browserifyWTK = browserify({
        entries: ['./assets/static/js/chat.js'],
        plugin: [watchify],
        debug: true,
        cache: {},
        packageCache: {},
        fullPaths: true
    });

    function handleErrors(error) {
        console.log('******  Start of Error  ******');
        console.log(error.message);
        console.log('******  End of Error  ******');
    }

    console.log("Eii Starting watcher");

    browserifyWTK.on('update', transformationWTK);

    transformationWTK();

    function transformationWTK() { // When any files update
        var updateStart = Date.now();
        console.log('Updating!');
        browserifyWTK.bundle()
            .on('error', handleErrors)
            .pipe(source('chat.js'))
            .pipe(gulp.dest('./assets/static/js/build/'));
        console.log('Updated!', (Date.now() - updateStart) + 'ms');
    }
});
