var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');


describe('Client a Chat test', function() {
    var chatview;
    var chat;
    var endChatSpy;
    var sendMessageSpy;
    var startChatSpy;

    beforeEach(function() {
        //Injecting empty JQUERY
        global.$ = function() {};

        //Injecting mock eureca
        global.Eureca = {
            Client: function() {}
        };
        sinon.stub(global.Eureca, 'Client', function() {
            return {
                ready: function() {},
                exports: {}
            }
        });

        //Initialise chatView, Object under testing
        chat = require('../assets/static/js/chat');
        chatview = new chat.chatview();

        chatview.server = {
            tchatServer: {
                startChat: function(name) {},
                send: function(message, name) {},
                endChat: function() {}
            }
        };
        endChatSpy = sinon.spy(chatview.server.tchatServer, 'endChat');
        sendMessageSpy = sinon.spy(chatview.server.tchatServer, 'send');
        startChatSpy = sinon.spy(chatview.server.tchatServer, 'startChat');

    });

    it('Client chat initialise the login name', function() {
        expect(chatview.loginName()).to.contain('anonymous-');
    });

    it('Client chat expose methods are built', function() {
        expect(chatview.tchat).to.an('object');
        expect(chatview.tchat.send).to.an('Function');
        expect(chatview.tchat.welcome).to.an('Function');
        expect(chatview.tchat.isGone).to.an('Function');
        expect(chatview.tchat.isConnected).to.an('Function');
    });
    it('Client chat send method when no client selected and no clients connected', function() {
        chatview.tchat.send("name", "message");

        expect(chatview.clientSelected()).to.be.null;
        expect(chatview.clients().length).to.equal(0);
    });
    it('Client chat send method when client is logged but not selected', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));

        chatview.tchat.send("testname", "message");

        expect(chatview.clientSelected()).to.be.null;
        expect(chatview.clients().length).to.equal(1);
        expect(chatview.clients()[0].messages().length).to.equal(1);
        expect(chatview.clients()[0].messages()[0].name).to.equal('testname');
        expect(chatview.clients()[0].messages()[0].message).to.equal('message');
        expect(chatview.clients()[0].messageNotSeen()).to.be.true;
    });
    it('Client chat send method when client is logged and selected', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));
        chatview.clientSelected(chatview.clients()[0]);

        chatview.tchat.send("testname", "message");

        expect(chatview.clients().length).to.equal(1);
        expect(chatview.clients()[0].messages().length).to.equal(1);
        expect(chatview.clients()[0].messages()[0].name).to.equal('testname');
        expect(chatview.clients()[0].messages()[0].message).to.equal('message');
        expect(chatview.clients()[0].messageNotSeen()).to.be.false;
    });

    it('Client chat welcome method changes is logged boolean', function() {
        expect(chatview.isLogged()).to.be.false;

        chatview.tchat.welcome();

        expect(chatview.isLogged()).to.be.true;
    });
    it('Client chat isGone method removes the client from clients logged array', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));

        chatview.tchat.isGone('id');

        expect(chatview.clients().length).to.equal(0);
    });

    it('Client chat isConnected adds client if not already in clients array', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));

        chatview.tchat.isConnected('anotherid', 'anothername');

        expect(chatview.clients().length).to.equal(2);
    });
    it('Client chat isConnected is not added if is already in clients array', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));

        chatview.tchat.isConnected('id', 'anothername');
        chatview.tchat.isConnected('anotherid', 'testname');

        expect(chatview.clients().length).to.equal(1);
    });
    it('Chat View quitChat sets isLogged to false, removes selected client and tells server to end chat', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));
        chatview.clientSelected(chatview.clients()[0]);

        chatview.quitChat();

        expect(chatview.isLogged()).to.be.false;
        expect(chatview.clientSelected()).to.be.null;
        expect(endChatSpy.calledOnce).to.be.true;
    });

    it('Chat View send message when server is ready and message is not valid', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));
        chatview.clientSelected(chatview.clients()[0]);

        chatview.sendMessage();

        expect(chatview.errorNoMessage()).to.be.true;
        expect(chatview.errorNoClient()).to.be.false;
        expect(sendMessageSpy.called).to.be.false;
    });
    it('Chat View send message when server is ready and no client selected', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));

        chatview.sendMessage();

        expect(chatview.errorNoMessage()).to.be.false;
        expect(chatview.errorNoClient()).to.be.true;
        expect(sendMessageSpy.called).to.be.false;
    });
    it('Chat View send message when server is ready and client selected and message is valid', function() {
        chatview.clients.push(new chat.clientLogged('id', 'testname'));
        chatview.clientSelected(chatview.clients()[0]);
        chatview.messageToSend('Message to be sent');

        chatview.sendMessage();

        expect(chatview.errorNoMessage()).to.be.false;
        expect(chatview.errorNoClient()).to.be.false;
        expect(sendMessageSpy.calledOnce).to.be.true;
    });

    it('Chat View login when server ready', function() {
        chatview.login();

        expect(startChatSpy.calledOnce).to.be.true;
    });

});
