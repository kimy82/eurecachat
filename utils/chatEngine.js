/// <reference path="../customTypings/customTypings.d.ts"/>
"use strict";
var TchatEngine = (function () {
    function TchatEngine(connectionsHolder, disconnectEventEmitter) {
        var _this = this;
        this.startChat = function (name) {
            var realThis = eval('this');
            console.log('Client %s auth with %s', realThis.connection.id, name);
            if (name !== undefined) {
                var client = _this.connectionsHolder.connections[realThis.connection.id];
                client.clientName = name;
                client.eurecaClient.tchat.welcome();
                _this.notifyIsConnected(realThis.connection.id, name);
            }
        };
        this.send = function (message, name) {
            var realThis = eval('this');
            var sender = _this.connectionsHolder.connections[realThis.connection.id];
            for (var key in _this.connectionsHolder.connections) {
                var c = _this.connectionsHolder.connections[key];
                if (c.clientName == name || c.clientName == sender.clientName)
                    c.eurecaClient.tchat.send(sender.clientName, message);
            }
        };
        this.endChat = function () {
            var realThis = eval('this');
            _this.notifyIsGone(realThis.connection.id);
        };
        this.configureDisconnetEvent = function () {
            _this.disconnectEventEmitter.on("isGone", _this.notifyIsGone);
        };
        this.notifyIsGone = function (id) {
            for (var key in _this.connectionsHolder.connections) {
                var c = _this.connectionsHolder.connections[key];
                var remote = c.eurecaClient;
                remote.tchat.isGone(id);
            }
        };
        this.notifyIsConnected = function (id, name) {
            var client = _this.connectionsHolder.connections[id];
            for (var key in _this.connectionsHolder.connections) {
                var c = _this.connectionsHolder.connections[key];
                console.log("Notifiying connections c " + c);
                if (c.id != client.id) {
                    //Notify all connectied clients that a new one exist
                    c.eurecaClient.tchat.isConnected(id, name);
                    //Notify the new client about other clients
                    if (c.clientName != 'Annonymous')
                        client.eurecaClient.tchat.isConnected(c.id, c.clientName);
                }
            }
        };
        console.log("Tchat engine Initialise.....");
        this.connectionsHolder = connectionsHolder;
        this.disconnectEventEmitter = disconnectEventEmitter;
        this.configureDisconnetEvent();
    }
    return TchatEngine;
}());
exports.__esModule = true;
exports["default"] = TchatEngine;
//# sourceMappingURL=chatEngine.js.map