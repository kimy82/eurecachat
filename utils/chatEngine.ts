/// <reference path="../customTypings/customTypings.d.ts"/>

class TchatEngine implements TchatEngine{
    private connectionsHolder: ConnectionsHolder;
    public disconnectEventEmitter: EventEmitter;

    constructor(connectionsHolder: ConnectionsHolder, disconnectEventEmitter: EventEmitter) {
        console.log("Tchat engine Initialise.....");
        this.connectionsHolder = connectionsHolder;
        this.disconnectEventEmitter = disconnectEventEmitter;
        this.configureDisconnetEvent();
    }

    public startChat = (name: string) => {
        var realThis: thisConnection = eval('this');
        console.log('Client %s auth with %s', realThis.connection.id, name);

        if (name !== undefined) {
            let client: Client = this.connectionsHolder.connections[realThis.connection.id];
            client.clientName = name;
            client.eurecaClient.tchat.welcome();
            this.notifyIsConnected(realThis.connection.id, name);
        }
    }
    public send = (message: string, name:string) => {
        var realThis: thisConnection = eval('this');
        var sender = this.connectionsHolder.connections[realThis.connection.id];
        for (let key in this.connectionsHolder.connections) // just loop and send message to all connected clients
        {
            let c = this.connectionsHolder.connections[key];
            if (c.clientName == name || c.clientName == sender.clientName) //simulate authentication check
                c.eurecaClient.tchat.send(sender.clientName, message);
        }
    }
    public endChat = () => {
        var realThis: thisConnection = eval('this');
        this.notifyIsGone(realThis.connection.id);
    }

    private configureDisconnetEvent = () => {
      this.disconnectEventEmitter.on("isGone", this.notifyIsGone);
    }

    private notifyIsGone = (id: string) => {
        for (let key in this.connectionsHolder.connections) {
            let c = this.connectionsHolder.connections[key];
            var remote = c.eurecaClient;
            remote.tchat.isGone(id);
        }
    }

    private notifyIsConnected = (id: string, name: string) => {
        var client: Client = this.connectionsHolder.connections[id];
        for (var key in this.connectionsHolder.connections) {
            let c = this.connectionsHolder.connections[key];
            console.log("Notifiying connections c " + c)
            if (c.id != client.id) {
                //Notify all connectied clients that a new one exist
                c.eurecaClient.tchat.isConnected(id, name);
                //Notify the new client about other clients
                if(c.clientName != 'Annonymous')
                  client.eurecaClient.tchat.isConnected(c.id, c.clientName);
            }
        }
    }
}

export default TchatEngine;
