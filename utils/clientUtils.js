/// <reference path="../customTypings/customTypings.d.ts"/>
"use strict";
var ClientUtils = (function () {
    function ClientUtils() {
    }
    ClientUtils.prototype.getClient = function (eurecaClient, name, id) {
        return { eurecaClient: eurecaClient, clientName: name, id: id };
    };
    return ClientUtils;
}());
exports.__esModule = true;
exports["default"] = new ClientUtils();
//# sourceMappingURL=clientUtils.js.map