/// <reference path="../customTypings/customTypings.d.ts"/>

class ClientUtils {
    getClient(eurecaClient:any, name:string, id:string):Client {
        return {eurecaClient: eurecaClient, clientName: name, id: id};
    }
}

export default new ClientUtils();
