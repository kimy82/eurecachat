/// <reference path="../customTypings/customTypings.d.ts"/>
"use strict";
var clientUtils_1 = require('./clientUtils');
var ConnectionsHolder = (function () {
    function ConnectionsHolder(eurecaServer, disconnectEventEmitter) {
        var _this = this;
        this.connections = {};
        this.configure = function () {
            console.log("Configuring connections ...");
            _this.eurecaServer.onConnect(function (connection) {
                console.log('New client ', connection.id, connection.eureca.remoteAddress);
                var client = clientUtils_1["default"].getClient(_this.eurecaServer.getClient(connection.id), "Annonymous", connection.id);
                _this.connections[connection.id] = client;
            });
            _this.eurecaServer.onDisconnect(function (connection) {
                console.log('Client quit', connection.id);
                delete _this.connections[connection.id];
                _this.disconnectEventEmitter.emit('isGone', connection.id);
            });
        };
        this.eurecaServer = eurecaServer;
        this.configure();
        this.disconnectEventEmitter = disconnectEventEmitter;
    }
    return ConnectionsHolder;
}());
exports.__esModule = true;
exports["default"] = ConnectionsHolder;
//# sourceMappingURL=connectionsHolder.js.map