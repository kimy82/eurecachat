/// <reference path="../customTypings/customTypings.d.ts"/>

import clientUtils from './clientUtils';

class ConnectionsHolder implements ConnectionsHolder {
    public eurecaServer: EurecaServer;
    public connections: { [key: string]: Client; } = {};
    public disconnectEventEmitter: EventEmitter;

    constructor(eurecaServer: EurecaServer, disconnectEventEmitter: EventEmitter) {
        this.eurecaServer = eurecaServer;
        this.configure();
        this.disconnectEventEmitter = disconnectEventEmitter;
    }

    public configure = () => {
        console.log("Configuring connections ...");
        this.eurecaServer.onConnect((connection: Connection) => {
            console.log('New client ', connection.id, connection.eureca.remoteAddress);
            let client = clientUtils.getClient(
                this.eurecaServer.getClient(connection.id),
                "Annonymous",
                connection.id);
            this.connections[connection.id] = client;
        });
        this.eurecaServer.onDisconnect((connection: Connection) => {
            console.log('Client quit', connection.id);
            delete this.connections[connection.id];
            this.disconnectEventEmitter.emit('isGone', connection.id);
        });
    };
}

export default ConnectionsHolder;
